import base64
import logging
import re

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

MAX_MEMORY_USED = "max_memory_used"


def extract_metrics_from_log(log_lines_base64):
    log_lines = base64.b64decode(log_lines_base64).decode("utf-8")
    matches = re.search(r"REPORT.*\tMax Memory Used: (\d*) (\w*)\t.*", log_lines,)
    max_memory_used = int(matches.group(1))
    memory_unit = matches.group(2)
    LOGGER.info("Memory usage was %s %s", max_memory_used, memory_unit)
    if memory_unit == "GB":
        max_memory_used = max_memory_used * 1024

    return {MAX_MEMORY_USED: max_memory_used}
