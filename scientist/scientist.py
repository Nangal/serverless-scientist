import base64
import json
import logging
import os
import random
import requests
import time

from datetime import datetime
from datetime import timedelta
from threading import Thread

import boto3
import botocore
import yaml

from dynamodb_json import json_util as dyndb_json_util
from metrics_publisher import MetricsPubisher
import scientist_utils

from result_store import RunResultsStore

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

AWSREGION = os.environ.get("AWSREGION")

EXPERIMENTS_REFRESH_MINUTES = int(os.environ.get("EXPERIMENTS_REFRESH_MINUTES", "1"))
experiments_update_ts = datetime.now() - timedelta(
    minutes=2 * EXPERIMENTS_REFRESH_MINUTES
)
experiments = {}

EXPERIMENTS_BUCKET = os.environ.get("EXPERIMENTS_BUCKET")
EXPERIMENTOR_ARN = os.environ.get("EXPERIMENTOR_ARN")
RESULT_COLLECTOR_ARN = os.environ.get("RESULT_COLLECTOR_ARN")
RESPONSE_TIME = "response_time"
SCIENTIST_ADDED_RESPONSE_TIME = "scientists_added_response_time"

# Initialize AWS Clients once here and not in the lambda_handler
# to reduced the overhead time. Initializing
# the AWS Clients takes between 3-45ms.
LAMBDA_CLIENT = boto3.client("lambda")
DYNAMODB_CLIENT = boto3.client("dynamodb")
COUNTERS_TABLE = os.environ["COUNTERS_TABLE"]


def list_experiment_files(bucket_name, s3_client):
    """
        Retrieve list of experiments YAML files.
    """

    object_details = []

    paginator = s3_client.get_paginator("list_objects_v2")
    page_iterator = paginator.paginate(Bucket=bucket_name)

    for page in page_iterator:
        object_details.extend(page["Contents"])

    return [
        s3_object["Key"]
        for s3_object in object_details
        if not s3_object["Key"].endswith("/")
    ]


def retrieve_experiments(aws_account_id):

    LOGGER.info("Retrieving experiments from S3")

    s3_client = boto3.client("s3")

    experiment_files = list_experiment_files(EXPERIMENTS_BUCKET, s3_client)
    tmp_file = "/tmp/experiments.yaml"

    experiments = {}
    for file in experiment_files:
        s3_client.download_file(EXPERIMENTS_BUCKET, file, tmp_file)

        with open(tmp_file, "r") as experiments_file:
            expriments_str = experiments_file.read()
            expriments_str = expriments_str.replace(
                "{AWSACCOUNT_ID}", aws_account_id
            ).replace("{AWSREGION}", AWSREGION)
            experiment_details = yaml.load(expriments_str)
            experiments.update(experiment_details["experiments"])

    LOGGER.info("Retrieved experiments from S3: %s", experiments)

    return experiments


def find_experiment(experiments, path):

    for experiment, details in experiments.items():
        if details["path"] == path:
            return experiment, details

    raise ValueError("No experiment found for path: {}".format(path))


def execute_control(experiment_run, control_arn, experiment_details, experiment_name):
    LOGGER.info("invoking control")
    # Run the control
    start_control = time.time()

    response = LAMBDA_CLIENT.invoke(
        FunctionName=control_arn,
        InvocationType="RequestResponse",
        Payload=json.dumps(experiment_run["payload"]),
        LogType="Tail",
    )
    control_duration = int((time.time() - start_control) * 1000)

    metrics = scientist_utils.extract_metrics_from_log(response["LogResult"])

    LOGGER.info("Execution of control took %s ms", control_duration)
    metrics[RESPONSE_TIME] = control_duration

    response_body = response["Payload"].read().decode("utf-8")

    control_response = json.loads(response_body)

    # Report results of control
    run_result = {
        "run_type": "control",
        "implementation_name": experiment_details["control"]["name"],
        "arn": control_arn,
        "experiment_name": experiment_name,
        # Original Payload value is a stream, replace it with the real contents
        "received_response": control_response,
        "metrics": metrics,
        "comparators": experiment_run["comparators"],
    }

    return run_result


def invoke_experimentor(experiment_run):
    # Hand over to Experimentor to run candidates (asynchonously)
    LOGGER.info("invoking experimentor")
    start_expirimentor = time.time()

    LAMBDA_CLIENT.invoke(
        FunctionName=EXPERIMENTOR_ARN,
        InvocationType="Event",
        Payload=json.dumps(experiment_run),
    )
    LOGGER.info(
        "Posting to experimentor took %s ms",
        int((time.time() - start_expirimentor) * 1000),
    )


def get_run_id():

    LOGGER.info("Generating new run_id")
    start = time.time()

    try:
        response = DYNAMODB_CLIENT.update_item(
            TableName=COUNTERS_TABLE,
            Key={"counter_id": {"S": "run_id"}},
            UpdateExpression="SET last_run_id = last_run_id + :incr",
            ExpressionAttributeValues={":incr": {"N": "1"}},
            ReturnValues="UPDATED_NEW",
        )

        response = dyndb_json_util.loads(response)
        run_id = response["Attributes"]["last_run_id"]

        duration = int((time.time() - start) * 1000)
        LOGGER.info("Getting next run_id (%s) took %sms", run_id, duration)

        LOGGER.info("Generated new run_id %s", run_id)
        return run_id
    except botocore.exceptions.ClientError:
        response = DYNAMODB_CLIENT.update_item(
            TableName=COUNTERS_TABLE, Key={"counter_id": {"S": "run_id"}}
        )
        response = dyndb_json_util.loads(response)
        if not "Item" in response:
            start_run_id = 100000
            LOGGER.info("Initializing run_id counter")
            DYNAMODB_CLIENT.put_item(
                TableName=COUNTERS_TABLE,
                Item={
                    "counter_id": {"S": "run_id"},
                    "last_run_id": {"N": str(start_run_id)},
                },
            )
            LOGGER.info("Generated new start_run_id %s", start_run_id)
            return start_run_id


def base64decode_event_body(event):
    if "isBase64Encoded" in event and event["isBase64Encoded"]:
        if "body" in event and event["body"]:
            event["body"] = base64.b64decode(event["body"]).decode("utf-8")
            event["isBase64Encoded"] = False

    return event


def lambda_handler(event, response_url):
    global experiments_update_ts
    global experiments
    LOGGER.info("event: %s", event)
    LOGGER.info("response_url: %s", response_url)
    if "keephot" in event and event["keephot"]:
        requests.post(
            response_url,
            data=json.dumps({"statusCode": 200, "body": "I'm still warm."}),
        )
        return

    start = time.time()

    if experiments_update_ts < datetime.now() - timedelta(
        minutes=EXPERIMENTS_REFRESH_MINUTES
    ):
        aws_account_id = event["requestContext"]["accountId"]
        experiments = retrieve_experiments(aws_account_id)
        experiments_update_ts = datetime.now()

    experiment_path_part = event["path"].replace("/scientist", "")
    if len(experiment_path_part) > 1:
        experiment_path_part = experiment_path_part[1:]
    if not experiment_path_part:
        # scientist invoked without experiment mentioned
        # Just return list of available experiments
        host = event["headers"]["Host"]
        body_text = "The following experiments are available:\n"
        for experiment in experiments:
            experiment_path = experiments[experiment]["path"]
            body_text = f"{body_text}https://{host}/v1/scientist/{experiment_path}\n"

        requests.post(
            response_url,
            data=json.dumps(
                {
                    "statusCode": 200,
                    "body": body_text,
                    "headers": {"Content-Type": "text/plain"},
                }
            ),
        )
        return

    experiment_name, experiment_details = find_experiment(
        experiments, experiment_path_part
    )
    control_arn = experiment_details["control"]["arn"]

    experiment_run = {
        "candidates": experiment_details["candidates"],
        "payload": base64decode_event_body(event),
        # Because of binary asset setting in API Gateway, body comes encoded in base 64.
        # So, if that's the case, decode the body first.
        "experiment_name": experiment_name,
        "comparators": experiment_details["comparators"],
    }

    control_run_result = execute_control(
        experiment_run, control_arn, experiment_details, experiment_name
    )

    requests.post(
        response_url, data=json.dumps(control_run_result["received_response"])
    )
    LOGGER.info(
        "Posting of response done after %sms: %s",
        int((time.time() - start) * 1000),
        control_run_result["received_response"],
    )

    # run_id is determined here such that it is done after the control
    # has been invoked and does not add latency.
    experiment_run["run_id"] = get_run_id()
    control_run_result["run_id"] = experiment_run["run_id"]
    control_run_result["request_payload"] = experiment_run["payload"]

    scientist_duration = int((time.time() - start) * 1000)
    metrics_publisher = MetricsPubisher()

    overhead = scientist_duration - control_run_result["metrics"][RESPONSE_TIME]
    metrics_publisher.publish_value(
        SCIENTIST_ADDED_RESPONSE_TIME,
        experiment_name,
        "scientist-overhead",
        "",
        overhead,
    )
    LOGGER.warning("Overhead of scientist %s ms", overhead)

    results_store = RunResultsStore()
    results_store.store_result(control_run_result)
    invoke_experimentor(experiment_run)
    LOGGER.info("Returning after %s ms", (time.time() - start) * 1000)
