import base64
import json
import logging
import os

import boto3

from result_store import RunResultsStore

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


def json_to_base64_string(json_obj):
    return base64.b64encode(
        bytearray(json.dumps(json_obj, sort_keys=True, indent=4), encoding="utf-8")
    ).decode("utf-8")


def lambda_handler(event, context):
    LOGGER.info("Event: %s", event)

    run_id = int(event["queryStringParameters"]["run_id"])
    LOGGER.info("Showing diff for %s", run_id)

    results_store = RunResultsStore()
    run_results = results_store.get_run_results(run_id)

    candidates_to_compare = []
    for run_result in run_results:
        if run_result["run_type"] == "control":
            control_response = run_result["received_response"]
            control_implementation_name = run_result["implementation_name"]
            request_payload = run_result["request_payload"]
        else:
            candidate_to_compare = {}
            candidate_to_compare["run_type"] = run_result["run_type"]
            candidate_to_compare["received_response"] = run_result["received_response"]
            candidate_to_compare["implementation_name"] = run_result[
                "implementation_name"
            ]
            candidates_to_compare.append(candidate_to_compare)

    candidate_responses_html = ""
    with open("./diff.tmpl.html", "r") as content_file:
        template = content_file.read()
        template = template.replace(
            "{{REQUEST_PAYLOAD}}", json.dumps(request_payload, sort_keys=True, indent=4)
        )
        template = template.replace(
            "{{CONTROL_RESPONSE}}",
            json.dumps(control_response, sort_keys=True, indent=4),
        )
        template = template.replace(
            "{{CONTROL_IMPLEMENTATION_NAME}}", control_implementation_name
        )

        for candidate_to_compare in candidates_to_compare:
            candidate_response = json_to_base64_string(
                candidate_to_compare["received_response"]
            )
            candidate_responses_html += f"""
            <h4>Candidate: {candidate_to_compare["implementation_name"]}</h4>
            <pre class="" id="candidate-{candidate_to_compare["implementation_name"]}"></pre>
            <script>
                document.getElementById("candidate-{candidate_to_compare["implementation_name"]}").innerHTML = diffString(
                    atob("{json_to_base64_string(control_response)}"),
                    atob("{candidate_response}")
                );
            </script>
            """

    template = template.replace("{{CANDIDATE_RESPONSES}}", candidate_responses_html)

    return {
        "statusCode": 200,
        "body": template,
        "headers": {"Content-Type": "text/html"},
    }

