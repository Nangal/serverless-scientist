import json
import importlib
import logging
import os
import sys
import time

import boto3

from metrics_publisher import MetricsPubisher
from result_store import RunResultsStore

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

CANDIDATE_COMPARE_TRIGGERED_TABLE = os.environ["CANDIDATE_COMPARE_TRIGGERED_TABLE"]
MAX_MEMORY_USED = "max_memory_used"
RESPONSE_TIME = "response_time"
REQUEST_COUNT = "request_count"


def make_header_keys_lowercase(response):

    result = response.copy()

    if "headers" in response["received_response"]:
        headers = set(response["received_response"]["headers"].keys())
        for header in headers:
            result["received_response"]["headers"][header.lower()] = response[
                "received_response"
            ]["headers"][header]

    return result


def do_comparisons(control, candidate, comparators, metrics_publisher):

    control = make_header_keys_lowercase(control)
    candidate = make_header_keys_lowercase(candidate)

    for comparator in comparators:
        try:
            comparator_name, *rest = comparator.keys()
            cur_dir = os.path.dirname(__file__)
            table_dir = os.path.join(cur_dir, "comparators")
            sys.path.insert(1, table_dir)
            comparator_module = importlib.__import__(comparator_name)

            comparator_details = []
            if comparator[comparator_name]:
                comparator_details = comparator[comparator_name]

            result = comparator_module.compare(control, candidate, comparator_details)
            if not result:
                metric_name = metrics_publisher.failure_name(comparator_name)
                metrics_publisher.publish_failure(
                    f"{metric_name}_run_id",
                    control["experiment_name"],
                    candidate["run_type"],
                    candidate["implementation_name"],
                    control["run_id"],
                )

        except:
            LOGGER.exception("Error while executing comparison")


def publish_run_metric(metrics_publisher, run_result, mentric_name):
    metrics_publisher.publish_value(
        mentric_name,
        run_result["experiment_name"],
        run_result["run_type"],
        run_result["implementation_name"],
        int(run_result["run_metrics"][mentric_name]),
    )


def compare_experiment_results(run_id):

    LOGGER.info("compare_experiment_results for run_id: %s", run_id)

    metrics_publisher = MetricsPubisher()
    results_store = RunResultsStore()
    run_results = results_store.get_run_results(run_id)

    control_list = [result for result in run_results if result["run_type"] == "control"]
    if control_list:
        control = control_list[0]
        publish_run_metric(metrics_publisher, control, RESPONSE_TIME)
        publish_run_metric(metrics_publisher, control, MAX_MEMORY_USED)

        candidate_list = [
            result for result in run_results if result["run_type"] == "candidate"
        ]

        comparators = {}
        if "comparators" in control:
            comparators = control["comparators"]

        for candidate in candidate_list:
            publish_run_metric(metrics_publisher, candidate, RESPONSE_TIME)
            publish_run_metric(metrics_publisher, candidate, MAX_MEMORY_USED)

            LOGGER.debug("candidate: %s", candidate)
            LOGGER.debug("comparators: %s", comparators)
            do_comparisons(control, candidate, comparators, metrics_publisher)
