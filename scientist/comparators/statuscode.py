import logging

from metrics_publisher import MetricsPubisher

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

STATUS_CODE_EQUALITY = "status_code_equality"


def compare(control, candidate, arguments_list=None):

    LOGGER.debug("statuscode.compare() arguments_list %s:", arguments_list)

    metrics_publisher = MetricsPubisher()

    control_statuscode = None
    candidate_statuscode = None
    if "statusCode" in control["received_response"]:
        control_statuscode = control["received_response"]["statusCode"]
    if "statusCode" in candidate["received_response"]:
        candidate_statuscode = candidate["received_response"]["statusCode"]
    if control_statuscode == candidate_statuscode:
        metric_name = metrics_publisher.success_name(STATUS_CODE_EQUALITY)
    else:
        LOGGER.info(
            "Statuscode comparison failed: control_statuscode: %s candidate_statuscode: %s",
            control_statuscode,
            candidate_statuscode,
        )
        metric_name = metrics_publisher.failure_name(STATUS_CODE_EQUALITY)

    metrics_publisher.publish_fact(
        metric_name,
        control["experiment_name"],
        candidate["run_type"],
        candidate["implementation_name"],
    )

    return control_statuscode == candidate_statuscode

