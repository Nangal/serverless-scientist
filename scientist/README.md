# README

The scientist project assumes that you have control and candidate lambda's deployed in the
same AWS account as the account in which you will deploy the Scientist. If you just want
to run some examples, then please deploy the lambda's in the `samples/controls` and
`samples/candidates` directory.

Before deploying the scientist project you can add your experiments to the
`experiment_definitions` directory. This directory contains one sample file. Create a copy of
this file and adjust it for your experiments. The file will be copied to the exxperiments
S3 bucket on each `sls deploy`. Alternatively you can also manually upload experiment
defintion files to the `scientist-experiments-xxxxxx` bucket.

After that you can do an `sls deploy --profile [your aws profile]` which will deploy the
lamba's that make up the Scientist.

Once deployed your client can invoke the `control` via the Scientist enpoint (see url as
output of `sls deploy` command).

For example:

`curl https://xs1p46n123.execute-api.eu-west-1.amazonaws.com/round\?number\=0.2`

To add additional experiments, you can drop experiment definition files to the S3 bucket directly

# Comparing candidates against control

The `result_comparator` module compares the responses coming from the candidates with the response
received from the control. It can do this comparison on different aspects and for each experiment
it is configurable which comparisons must be executes. This is defined in the experiments YAML file
like this:

```
experiments:
  round:
    comparators:
      - body:
      - statuscode:
      - headers:
        - content-type
    path: round
    control:
      name: Round NodeJS12.x # TODO Also add `description:` entry to these.
      arn: arn:aws:lambda:eu-west-1:108211808302:function:control-round
    candidates:
      candidate-1:
        name: Round python-3-math
        arn: arn:aws:lambda:eu-west-1:108211808302:function:candidate-round-python3-math
```
In the above experiment comparisons are done on `body` content, `statuscode` and `headers`. Optionally
you can specify details to take into account when executing a comparison. In the above example
it states that for `headers` only the `content-type` header should be compared.

The comparator names (in this case `body`, `statuscode` and `headers`) are implemented in the
[`comparators` module](https://gitlab.com/practicalarchitecture/lamdba-scientist/tree/master/scientist/comparators).
The name of the sub modules in the comparator module must match the names used in the experiments yaml file.
Each comparator must implement this method: `def compare(control, candidate, arguments_list=None):`
The implementation must:
- Execute the comparison
- Publish the result as metrics

In future we will also implement the possibility to configure comparators that are implemented as Lambda
function which makes it possible to plug in your own comparators.

# Dashboards

Graphana and is used to visualize the results of the experiments done by Serverless Scientist.
Metrics for an experiment are posted to ElasticSearch and Grafana hosts the dashboards that visualize the results.

The hostname of Grafana can be found:

* as output of the Serverless Scientist project in the AWS Cloudformation console
(`GrafanaHost`) or,
* by executing a `aws cloudformation describe-stacks --stack-name serverless-scientist-v1 --query "Stacks[0].Outputs[?OutputKey=='GrafanaHost'].OutputValue" --output text` command (assumes you have the `aws cli` installed).

Grafana can be accessed at https://<GrafanaHost>:3000, username is `admin` and password is  set in the
docker-compose file as `GF_SECURITY_ADMIN_PASSWORD`.

Both ElasticSearch as a data source and a basis Dashboard are automatically provisioned, see the `grafana_provisioning` subdirectory.

** Deployment
Basic deployment is done by the `sls deploy` command. After that the step in  `provision_metrics_instance.sh` script must be executed. It will copy provisioning files to the EC2 instance and start the docker-compose.

Notes: sometimes it takes a while before DNS names are resolvable. The last commands in the script may then fail and you can
invoke them manually. The `provision_metrics_instance.sh` does contain a command to flush the DNS cache which addresses this.
However, this does not address DNS caching by the Lambda's so if they fail to post metrics, then you may need to undeploy and redeploy these.
