import json
import logging
import os
import time

import boto3

from result_store import RunResultsStore
import result_comparator
import scientist_utils

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

LAMBDA_CLIENT = boto3.client("lambda")


def lambda_handler(event, context):
    LOGGER.info("Event: %s", event)

    if "keephot" in event and event["keephot"]:
        return {"statusCode": 200, "body": "I'm still warm."}

    candidates = event["candidates"]
    experiment_name = event["experiment_name"]
    run_id = event["run_id"]
    results_store = RunResultsStore()

    for candidate_id in candidates:
        LOGGER.info(
            "Running candidate %s for experiment %s, run_id %s",
            candidate_id,
            experiment_name,
            run_id,
        )
        candidate_details = candidates[candidate_id]

        try:
            start = time.time()
            response = LAMBDA_CLIENT.invoke(
                FunctionName=candidate_details["arn"],
                InvocationType="RequestResponse",
                Payload=json.dumps(event["payload"]),
                LogType="Tail",
            )
            duration = int((time.time() - start) * 1000)

            metrics = scientist_utils.extract_metrics_from_log(response["LogResult"])
            metrics["response_time"] = duration

            # Report results of candidate
            run_result = {}
            run_result["request_payload"] = event["payload"]
            run_result["run_id"] = event["run_id"]
            run_result["run_type"] = "candidate"
            run_result["implementation_name"] = candidate_details["name"]
            run_result["arn"] = candidate_details["arn"]
            run_result["experiment_name"] = event["experiment_name"]
            # Original Payload value is a stream, replace it with the real contents
            response_body = response["Payload"].read().decode("utf-8")
            response = json.loads(response_body)
            run_result["received_response"] = response
            run_result["metrics"] = metrics
            run_result["comparators"] = event["comparators"]
            results_store.store_result(run_result)
        except LAMBDA_CLIENT.exceptions.ResourceNotFoundException:
            LOGGER.warning(
                "Got ResourceNotFoundException when invoking %s",
                candidate_details["arn"],
            )

        result_comparator.compare_experiment_results(run_id)
    return {"statusCode": 200}
