'use strict';

module.exports.handleGet = function(event, context, callback) {
  console.log("[INFO] handleGet() started, event data: ", event)

  var pathParameters = event["pathParameters"]
  var queryStringParameters = event["queryStringParameters"]
  // HTTP GET requests should ignore body parameter.

  console.log("[INFO] request path parameters: ", pathParameters)
  console.log("[INFO] request query string parameters: ", queryStringParameters)

  // TODO Implement your code here.

  const response = {'statusCode': 501,  // Not Implemented.
                    'headers': {'Content-Type': 'text/plain'},        
                    'body': "501 Not Implemented"
  }
  
  console.log("[INFO] response: ", response)
  callback(null, response)
}


// module.exports.handlePost = function(event, context, callback) {
//   console.log("[INFO] handlePost() started, event data: ", event)

//   var pathParameters = event["pathParameters"]
//   var queryStringParameters = event["queryStringParameters"]
//   var body = event["body"]

//   console.log("[INFO] request path parameters: ", pathParameters)
//   console.log("[INFO] request query string parameters: ", queryStringParameters)
//   console.log("[INFO] request body: ", body)

//   // TODO Implement your code here.

//   const response = {'statusCode': 501,  // Not Implemented.
//                     'headers': {'Content-Type': 'application/json'},        
//                     'body': JSON.stringify({"message": "501 Not Implemented"})
//   }
  
//   console.log("[INFO] response: ", response)
//   callback(null, response)
// }
