package com.serverlessscientist;

import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Handler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

	private static final Logger LOG = Logger.getLogger(Handler.class);

	@Override
	@SuppressWarnings("unchecked")
	public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
    LOG.info("handleRequest() started, event data: " + input);

		Map<String, String> pathParameters = new LinkedHashMap<>();
		if (input.get("pathParameters") != null) {
			pathParameters.putAll( (LinkedHashMap<String,String>) input.get("pathParameters") );
		}
		LOG.info("request path parameters: " + pathParameters);

		Map<String, String> queryStringParameters = new LinkedHashMap<>();
		if (input.get("queryStringParameters") != null) {
			queryStringParameters.putAll( (LinkedHashMap<String, String>) input.get("queryStringParameters"));
		}
		LOG.info("request query string parameters: " + queryStringParameters);

		// HTTP GET requests should ignore body parameter.

		// TODO Implement your code here.

		// Then, make sure to include the proper `package` structure (from `serverless.yml`
		// in this `skeleton/` directory) in `serverless.yml` in the candidate directory.

		// Make sure that you're referring to the proper folder, target folder and .jar file.

		// Also, properly define the Java function handler in `serverless.yml` in 
		// the `candidates/` directory. For an example, look in the 
		// `serverless.yml` in this `skeleton/` directory as well.

		// Build the Java package from the Java package directory in the
		// `candidates/` directory with `mvn package`. Then deploy the candidate(s).

		String responseBody = "501 Not Implemented";

		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "text/plain");

		ApiGatewayResponse response =  ApiGatewayResponse.builder()
				.setStatusCode(501) // Not Implemented.
				.setRawBody(responseBody)
				.setHeaders(headers)
				.build();

    LOG.info("handleRequest() response: " + response);

		return response;
	}
}
