require 'json'
require 'logger'

# Set up basic logger.
LOGGER = Logger.new(STDOUT)
LOGGER.level = Logger::INFO


def handle_get(event:, context:)
  LOGGER.info("handle_get() started, event data: %s" % event)

  path_parameters = event["pathParameters"]
  query_string_parameters = event["queryStringParameters"]
  # HTTP GET requests should ignore body parameter.

  LOGGER.info("request path parameters: %s" % path_parameters)
  LOGGER.info("request query string parameters: %s" % query_string_parameters)

  # TODO Implement your code here.

  response = { "statusCode": 501,  # Not Implemented.
               "headers": {"Content-Type": "text/plain"},
               "body": "501 Not Implemented"
  }

  LOGGER.info("handle_get() response: %s" % response)
  response
end


# def handle_post(event:, context:)
#   LOGGER.info("handle_post() started, event data: %s" % event)

#   path_parameters = event["pathParameters"]
#   query_string_parameters = event["queryStringParameters"]
#   request_body = event["body"]

#   LOGGER.info("request path parameters: %s" % path_parameters)
#   LOGGER.info("request query string parameters: %s" % query_string_parameters)
#   LOGGER.info("request body: %s" % request_body)

#   # TODO Implement your code here.

#   response = { "statusCode": 501,  # Not Implemented.
#                "headers": {"Content-Type": "application/json"},
#                "body": JSON.dump({"message": "501 Not Implemented"})
#   }

#   LOGGER.info("handle_post() response: %s" % response)
#   response
# end
