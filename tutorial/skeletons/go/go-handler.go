package main

import (
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

/*
This is the structure of the structs for APIGatewayProxyRequest and APIGatewayProxyResponse.

type APIGatewayProxyRequest struct {
    Resource                        string                        `json:"resource"` // The resource path defined in API Gateway
    Path                            string                        `json:"path"`     // The url path for the caller
    HTTPMethod                      string                        `json:"httpMethod"`
    Headers                         map[string]string             `json:"headers"`
    MultiValueHeaders               map[string][]string           `json:"multiValueHeaders"`
    QueryStringParameters           map[string]string             `json:"queryStringParameters"`
    MultiValueQueryStringParameters map[string][]string           `json:"multiValueQueryStringParameters"`
    PathParameters                  map[string]string             `json:"pathParameters"`
    StageVariables                  map[string]string             `json:"stageVariables"`
    RequestContext                  APIGatewayProxyRequestContext `json:"requestContext"`
    Body                            string                        `json:"body"`
    IsBase64Encoded                 bool                          `json:"isBase64Encoded,omitempty"`
}

type APIGatewayProxyResponse struct {
    StatusCode        int                 `json:"statusCode"`
    Headers           map[string]string   `json:"headers"`
    MultiValueHeaders map[string][]string `json:"multiValueHeaders"`
    Body              string              `json:"body"`
    IsBase64Encoded   bool                `json:"isBase64Encoded,omitempty"`
}
*/

// HandleGetAndPost is the lambda handler invoked by the `lambda.Start` function call.
// Based on the HTTP method (GET or POST) it redirects control to a specific function,
// and returns the response as received.
func HandleGetAndPost(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var response events.APIGatewayProxyResponse
	var err error

	if request.HTTPMethod == "GET" {
		response, err = HandleGet(request)
	} else if request.HTTPMethod == "POST" {
		log.Printf("HTTP POST receveived, but handler function not yet enabled; taking HandleGet() for now.")
		response, err = HandleGet(request)
		// response, err = HandlePost(request)
	}

	return response, err
}

// HandleGet is the handler function for HTTP GET requests.
func HandleGet(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	log.Printf("HandleGet() started, request data: %v", request)

	var pathParameters = request.PathParameters
	var queryStringParameters = request.QueryStringParameters
	// HTTP GET requests should ignore body parameter.

	log.Printf("request path parameters: %v", pathParameters)
	log.Printf("request query string parameters: %v", queryStringParameters)

	// TODO Implement your code here.

	// Don't forget to copy `Makefile` as well, and build this code
	// with `make build` before deploying Go handler to AWS Lambda.

	var response = events.APIGatewayProxyResponse{
		StatusCode: 501,
		Headers:    map[string]string{"Content-Type": "text/plain"},
		Body:       "501 Not Implemented"}

	log.Printf("HandleGet() response: %v", response)
	return response, nil
}

// HandlePost is the handler function for HTTP POST requests.

// func HandlePost(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
// 	log.Printf("HandlePost() started, request data: %v", request)

// 	var pathParameters = request.PathParameters
// 	var queryStringParameters = request.QueryStringParameters
// 	var body = request.Body

// 	log.Printf("request path parameters: %v", pathParameters)
// 	log.Printf("request query string parameters: %v", queryStringParameters)
// 	log.Printf("request body: %v", body)

// 	// TODO Implement your code here.

// 	var message, _ = json.Marshal(map[string]string{"message": "501 Not Implemented"})

// 	var response = events.APIGatewayProxyResponse{
// 		StatusCode: 501,
// 		Headers:    map[string]string{"Content-Type": "application/json"},
// 		Body:       string(message)}

// 	log.Printf("HandleGet() response: %v", response)
// 	return response, nil
// }

func main() {
	lambda.Start(HandleGetAndPost)
}
