import json
import time
import string
import random
import logging
# import requests  # Not available at AWS Lambda.
# from botocore.vendored import requests  # Deprecation notice at AWS Lambda.

import urllib3

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


# Defaults for target URL, number of requests and delay between requests.
TARGET_URL = ""  # This is silly default... but what else?
NR_REQUESTS = 10  # Default: 10 requests each with 5 second delay.
DELAY = 5
MAX_TIME = 120  # Just set some maximum time for traffic burst: 2 minutes.


def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    # Fetch parameters for traffic from ENV.
    try:
        url = event['url'] if event['url'] else TARGET_URL
        nr_requests = int(event['nr_requests']) if event['nr_requests'] else NR_REQUESTS
        delay = int(event['delay']) if event['delay'] else DELAY

        # Generate traffic to URL.
        generate_traffic(url, nr_requests, delay) 
    except:
        print("Error in retrieving URL, number of requests or delay.")

    # Returning a response object isn't necessary for a triggered Lambda function event.
    return

def generate_traffic(url, nr_requests, delay):
    LOGGER.info("generate_traffic() started, URL: %s", url)

    # Test that nr_requests * delay does not exceed MAX_TIME.
    if (nr_requests * delay > MAX_TIME):
        # If so, set them back to defaults.
        nr_requests = NR_REQUESTS
        delay = DELAY

    for _ in range(nr_requests):
        do_one_request(url)
        time.sleep(delay)


def do_one_request(url):
    LOGGER.info("do_one_request() started, URL: %s", url)

    # Get a random-sized list of random strings.
    nr_items = random.randint(10,50)
    unsorted_strings = []
    allowed_chars = string.ascii_letters + string.digits
    for _ in range(nr_items):
        string_length = random.randint(4,16)
        random_string = ''.join([random.choice(allowed_chars) for n in range(string_length)])
        unsorted_strings.append(random_string)

    # Construct request body.
    data = json.dumps(unsorted_strings)

    try:
        # Prepare the request and send it.
        LOGGER.info("HTTP POST to: %s", url)

        http = urllib3.PoolManager()
        response = http.request('POST',
                                url,
                                body = data,
                                headers = {'Content-Type': 'application/json'},
                                retries = False)

        LOGGER.info("response: %s", response)
    except:
        LOGGER.error("Error in HTTP POST to %s", url)
