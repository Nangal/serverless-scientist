import urllib.request
import time
import random
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

# Defaults for target URL, number of requests and delay between requests.
TARGET_URL = ""  # This is silly default... but what to choose?
NR_REQUESTS = 10  # Default: 10 requests each with 5 second delay.
DELAY = 5
MAX_TIME = 120  # Just set some maximum time for traffic burst: 2 minutes.


def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    # Fetch parameters for traffic from event data.
    try:
        url = event['url'] if event['url'] else TARGET_URL
        nr_requests = int(event['nr_requests']) if event['nr_requests'] else NR_REQUESTS
        delay = int(event['delay']) if event['delay'] else DELAY

        # Generate traffic to URL.
        generate_traffic(url, nr_requests, delay) 
    except:
        LOGGER.error("Error in retrieving URL, number of requests or delay.")

    # Returning a response object isn't necessary for a triggered Lambda function event.
    return


def generate_traffic(url, nr_requests, delay):
    LOGGER.info("generate_traffic() started, URL: %s", url)

    # Test that nr_requests * delay does not exceed MAX_TIME.
    if (nr_requests * delay > MAX_TIME):
        # If so, set them back to defaults.
        nr_requests = NR_REQUESTS
        delay = DELAY

    for _ in range(nr_requests):
        do_one_request(url)
        time.sleep(delay)


def do_one_request(url):
    LOGGER.info("do_one_request() started, URL: %s", url)

    # Get a random number in [0.0 ... 100.0] with two decimals.
    number = random.randint(0, 2000) / 20

    # Construct new url.
    url += "?number=" + str(number)

    try:
        LOGGER.info("HTTP GET to: %s", url)
        response = urllib.request.urlopen(url).read()
        LOGGER.info("response: %s", response)
    except:
        LOGGER.error("Error in HTTP GET from %s", url)
