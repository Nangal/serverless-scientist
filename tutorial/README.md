
# Prerequisites

In order to develop the Serverless Scientist track in Instruqt the folling is required:

- Instruqt account (create yourself using you `@xebia.com` mail address)
- Your Instruct account must be part of the Xebia organization (ask Adé)
- Google account tied to your `@xebia.com` account
- Google account must be included in Xebia Google organization (ask Adé)
- Install google cloud sdk (e.g. `brew cask install google-cloud-sdkz`)
- `gcloud auth configure-docker`
- `gcloud auth login`
- Instruqt CLI: https://docs.instruqt.com/#install-sdk
- `instruqt auth login`


# Building docker image for Instruqt

To run the track in Instruqt a custom docker image is used (see Dockerfile). This image provides:
- the CLI for the participants
- VSCode
- Link to AWS console
- Dependencies required to build and run the serverless scientist project

To build and deploy the image use the following commands:

```
docker build -t gcr.io/instruqt-xebia/serverless-scientist .
docker push gcr.io/instruqt-xebia/serverless-scientist:latest
```

# Developing track

The Serverless Scientist track is developed in Instruqt, refer to [Instruqt documentation](https://docs.instruqt.com) for details.

# Version control on track

Instruqt itself does not provide version control, so we have to manually export the track and include it out repo. In the
`tutorial` folder there is a `track` subfolder.

In the `track/serverless-scientist` subfolder use the following command to update to the latest version of the track:

```
instruqt track pull
```

After that do a normal `git add/commit/push` to include the track in Git.