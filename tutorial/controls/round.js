'use strict';

module.exports.lambdaHandler = function(event, context, callback) {
  console.log("[INFO] lambdaHandler() started, event data: ", event)

  var queryStringParameters = event["queryStringParameters"]

  // Set up response structure for default (failure) case.
  var response = { 'statusCode': 404,
                   'headers': {'Content-Type': 'text/plain'},        
                   'body': "Pass a valid number as query string parameter, e.g., `?number=4.2`.\n"}

  // TODO JSc Check if/how `round(-0)` is accepted and rounded.

  // Test whether query string parameter `number` is present, and a valid number.
  if (queryStringParameters &&
      queryStringParameters.number &&
      parseFloat(queryStringParameters.number)) {

    // Get the number from the query string, and round it.
    var number = parseFloat(queryStringParameters.number)
    var roundedNumber = doRound(number)

    // And build the response structure.
    response = { 'statusCode': 200,
                 'headers': {'Content-Type': 'text/plain'},        
                 'body': roundedNumber.toString() }
  }
  
  console.log("[INFO] response: ", response)
  callback(null, response)
}


function doRound(number) {
  return Math.round(number);
}
