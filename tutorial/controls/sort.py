import json
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event data: %s", event)

    request_body = event['body']
    LOGGER.info("request body: %s", request_body)

    # Set up the default response in case sorting the body doesn't work at all.
    response = { "statusCode": 400,  # Bad Request
                 "headers": {'Content-Type': 'application/json'},
                 "body": json.dumps({"message": "400 Bad Request"}) }

    if request_body:
      try:
        body = json.loads(request_body)
        try:
          sorted_body = do_sort(body)
          response = { "statusCode": 200,
                      "headers": {'Content-Type': 'application/json'},
                      "body": json.dumps(sorted_body, separators=(',', ':')) }
        except Exception:
          response = { "statusCode": 400,  # Bad Request
                      "headers": {'Content-Type': 'application/json'},
                      "body": json.dumps({"message": "Can't sort items of different types"}) }
      except Exception:
        response = { "statusCode": 400,  # Bad Request
                    "headers": {'Content-Type': 'application/json'},
                    "body": json.dumps({"message": "Invalid JSON structure"}) }

    LOGGER.info("lambda_handler() response: %s", response)
    return response


def do_sort(list):
    LOGGER.info("do_sort() started, list: %s", list)

    return sorted(list)