require 'logger'

# Set up basic logger.
LOGGER = Logger.new(STDOUT)
LOGGER.level = Logger::INFO

DEFAULT_FROM = 1
DEFAULT_LENGTH = 100

def lambda_handler(event:, context:)
  LOGGER.info("handle_get() started, event data: %s" % event)

  # We're using optional `from` and `to` query string parameters,
  # to specify the range of requested numbers.
  query_string_parameters = event["queryStringParameters"]
  LOGGER.info("request query string parameters: %s" % query_string_parameters)
 
  # Try to get the parameters from the query string.
  # Use defaults if not supplied in the request.
  if query_string_parameters && query_string_parameters["from"]
    # Make sure that sequence doesn't start lower than DEFFAULT_FROM.
    from = [query_string_parameters["from"].to_i, DEFAULT_FROM].max
  else
    from = DEFAULT_FROM
  end

  if query_string_parameters && query_string_parameters["to"]
    to = query_string_parameters["to"].to_i
  else
    to = from + DEFAULT_LENGTH - 1
  end

  string = fizzbuzz(from, to)

  response = { "statusCode": 200,
               "headers": {"Content-Type": "text/plain"},
               "body": string }

  LOGGER.info("handle_get() response: %s" % response)
  response
end

def fizzbuzz(from, to)
  from.upto(to).map do |n|
    case
      when n % 15 == 0 then "Fizz Buzz"
      when n % 3  == 0 then "Fizz"
      when n % 5  == 0 then "Buzz"
      else n
    end
  end.join("\n")
end
