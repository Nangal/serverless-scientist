Downloading Serverless Scientist
================================

Either download the repository of Serverless Scientist at
`https://gitlab.com/practicalarchitecture/serverless-scientist`
or clone the git repository via
`git clone https://gitlab.com/practicalarchitecture/serverless-scientist.git`

Installing Serverless Scientist
===============================

Move into the downloaded directory or cloned repository:
`cd serverless-scientist`.

In that folder, make sure that Serverless Framework and required
plugins are installed:

```
npm install serverless
npm install
npm install serverless-offline-python
npm install serverless-python-requirements
npm install serverless-s3-sync
npm install serverless-offline
npm install serverless-apigw-binary
npm install serverless-pseudo-parameter
npm install serverless-dynamodb-autoscaling
```

Serverless Scientist consist of four parts, each in in own directory
and as a separate Serverless service:
  * `scientist` - containing the Serverless Scientist code
  * `samples/controls` - a set of examples of control functions
  * `samples/candidates` - a set of candidates for experiments with those controls
  * `samples/traffic` - sample traffic generators towards the samples:
    controls, candidates or experiments

Sample controls
---------------

Deploy Serverless Scientist's sample controls via:

```
cd samples/controls
sls deploy --profile <profile>
```

This will result in something like:

```
service: samples-controls
stage: v1
region: eu-west-1
stack: samples-controls-v1
endpoints:
  GET - https://e20l302pp6.execute-api.eu-west-1.amazonaws.com/v1/round
  POST - https://e20l302pp6.execute-api.eu-west-1.amazonaws.com/v1/sort
  GET - https://e20l302pp6.execute-api.eu-west-1.amazonaws.com/v1/qrcode
functions:
  round: samples-controls-v1-round
  sort: samples-controls-v1-sort
  qrcode: samples-controls-v1-qrcode
layers:
  None
```

Sample experiments: candidates
------------------------------

Deploy Serverless Scientist's experiments with sample candidates via:

```
cd samples/candidates
sls deploy --profile <profile>
```

This will result in something like:

```
service: samples-candidates
stage: v1
region: eu-west-1
stack: samples-candidates-v1
endpoints:
  GET - https://o60q713ke4.execute-api.eu-west-1.amazonaws.com/v1/candidate/python3-round/round
  GET - https://o60q713ke4.execute-api.eu-west-1.amazonaws.com/v1/candidate/python3-math/round
  POST - https://o60q713ke4.execute-api.eu-west-1.amazonaws.com/v1/candidate/python3-sorted/sort
  POST - https://o60q713ke4.execute-api.eu-west-1.amazonaws.com/v1/candidate/python3-mergesort/sort
  POST - https://o60q713ke4.execute-api.eu-west-1.amazonaws.com/v1/candidate/go1-sort/sort
  GET - https://o60q713ke4.execute-api.eu-west-1.amazonaws.com/v1/candidate/node8-qr-image/qrcode
functions:
  round-python3-round: samples-candidates-v1-round-python3-round
  round-python3-math: samples-candidates-v1-round-python3-math
  sort-python3-sorted: samples-candidates-v1-sort-python3-sorted
  sort-python3-mergesort: samples-candidates-v1-sort-python3-mergesort
  sort-go1-sort: samples-candidates-v1-sort-go1-sort
  qrcode-node8-qr-image: samples-candidates-v1-qrcode-node8-qr-image
layers:
  None
```

Serverless Scientist
--------------------

Move into the Scientist directory: `cd scientist` and deploy Scientist:
` sls deploy --profile xebia --experiment-bucketpostfix <postfix>`

This will result in something like:

```
service: serverless-scientist
stage: v1
region: eu-west-1
stack: serverless-scientist-v1
endpoints:
  ANY - https://3j0ql2e4wc.execute-api.eu-west-1.amazonaws.com/v1/scientist
  ANY - https://3j0ql2e4wc.execute-api.eu-west-1.amazonaws.com/v1/scientist/{experiment}
functions:
  resultCollector: serverless-scientist-v1-resultCollector
  experimentor: serverless-scientist-v1-experimentor
  scientist: serverless-scientist-v1-scientist
  resultComparator: serverless-scientist-v1-resultComparator
layers:
  None
S3 Sync: Syncing directories and S3 prefixes...
...
S3 Sync: Synced.
```

Make sure to write down that REST API resource id of the API gateway;
`3j0ql2e4wc` in the example above.

Traffic generators
------------------

Serverless Scientist comes with a number of traffic generators that can
generate requests towards experiments (or individual sample control functions
and specific sample candidate functions). Each traffic generator is a
Lambda function that can be triggered, and on invocation executes a couple
of requests towards a configured API endpoint.

The traffic generators can be installed with:

```
cd samples/traffic
sls deploy --profile <profile> --apigateway <api_gateway_id>
```

This will result in something like:

```
service: samples-traffic
stage: v1
region: eu-west-1
stack: samples-traffic-v1
resources: 17
endpoints:
  None
functions:
  round-traffic: traffic-round-traffic
  sort-traffic: traffic-sort-traffic
  qrcode-traffic: traffic-qrcode-traffic
layers:
  None
```
