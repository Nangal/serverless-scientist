import sys
import os
import pyqrcode
import base64
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    # Create error response in case password is missing in query string.
    response = {
                "statusCode": 404,
                "body": "Supply a text string as query string parameter, e.g., `?text=what+you+want+to+encode+in+the+QR+code`."
               }

    if event['queryStringParameters'] and 'text' in event['queryStringParameters']:
        # Get password from event data.
        text = event['queryStringParameters']['text']

        # Generate QR code and encode it as base64 string.
        image_in_base64 = get_qrcode(text)

        # Build new response.
        response = {
                    "statusCode": 200,
                    "headers": { "Content-Type": "image/png" },
                    "body": image_in_base64,
                    "isBase64Encoded": "true"
                   }

    LOGGER.info("response: %s", response)
    return response

def get_qrcode(text):
    LOGGER.info("get_qrcode() started, text: %s", text)

    # Generate image.
    # img = pyqrcode.create(text)
    mode = 'binary'
    error_correction = 'M'
    img = pyqrcode.create(text, error = error_correction, mode = mode)

    # Set unit size to 4 pixels.
    scale = 4
    return img.png_as_base64_str(scale)
