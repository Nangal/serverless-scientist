var QR = require('qr-image')

exports.lambdaHandler = function (event, context, callback) {
  console.log("[INFO] lambdaHandler() started, event: " + event)

  // Prepare default response that'll be used when request is incorrect.
  response = {
      'statusCode': 404,
      'headers': {'Content-Type': 'application/json'},        
      'body': JSON.stringify({"error": "Missing arguments."})  // This is different than in control on purpose.
  }

  // Check required query string parameter `text`.
  if (!event.queryStringParameters ||
      !event.queryStringParameters.text) {
    callback(null, response)
    return
  }

  // Get text to be encoded in QR code.
  text = event.queryStringParameters.text

  // Set requested format, "png" as default (as only option for `qr-image` library).
  type = "png"

  // TODO In order to support JPG images (as the control is), consider using
  // an additional library to convert the image from PNG to JPG if required.

  // Set corresponding Content Type header.
  headers = {}
  headers["Content-Type"] = "image/png"

  // Get error correction level from request, taking "M" as default.
  ecl = "M" // Can be "L", "M", "Q" or "H".

  if (event.queryStringParameters.ecl) {
    // On purpose, don't check whether ECL is valid value.
    ecl = event.queryStringParameters.ecl.toUpperCase() 
  }

  // QR code will be stored in base64-encoded data URL.
  options = {}
  options["type"] = type
  options["ec_level"] = ecl

  options["size"] = 4  // Default value is 5, but then image becomes larger than control.

  // TODO Add `size` and `margin` options. This is where QR code can differ from control as well.
  // - size (png and svg only) — size of one module in pixels. Default 5 for png and undefined for svg.
  // - margin — white space around QR image in modules. Default 4 for png and 1 for others.

  image = QR.imageSync(text, options)

  // Build response.
  response = {
    // Necessary so that API Gateway transforms body to binary.
    'isBase64Encoded': true, 
    'statusCode': 200,
    'headers': headers,
    'body': image.toString('base64')
  }

  console.log("[INFO] response: " + response)

  callback(null, response)
};