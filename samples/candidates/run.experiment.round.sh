#! /bin/sh
# ----------------------------------------------------------
# Run experiment towards `round` AWS Lambda function.
# Uses `curl(1)` to send GET request to Lambda.
# ----------------------------------------------------------

# Handle arguments of command.
USAGE="Usage: $0 <number of tests>"
if [ $# == 0 ] ; then
  echo $USAGE
  exit 1;
fi
if ! [[ "$1" =~ ^[0-9]+$ ]]
  then
    echo $USAGE
    exit 1;
fi

# Number of requests.
times=$1

# Loop $times.
while [ $times -gt 0 ]; do
  # Generate random number between 0 and 100 with two digits.
  rand=$(echo "scale=2; $RANDOM/327.67"|bc)
  # Send number to Lambda function with HTTP GET.
  # TODO Obviously, refer only to publicly available URL of Lambda function,
  #      and let Scientist handle calling the candidates.
  /usr/bin/curl http://localhost:3000/round/control?number=$rand 2> /dev/null
  echo
  /usr/bin/curl http://localhost:3000/round/python-3-round?number=$rand 2> /dev/null
  echo
  /usr/bin/curl http://localhost:3000/round/python-3-math?number=$rand 2> /dev/null
  echo

  times=$(($times-1))
  sleep .2
done
