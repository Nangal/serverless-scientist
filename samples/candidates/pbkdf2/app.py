import hashlib
import binascii
import sys
import os
from datetime import datetime
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    # Create error response in case password is missing in query string.
    response = {
                "statusCode": 404,
                "headers": {'Content-Type': 'text/plain'},
                "body": "Supply a password as query string parameter, e.g., `?password=correct+horse+battery+staple`."
               }

    if event['queryStringParameters'] and 'password' in event['queryStringParameters']:
        # Get password from event data.
        password = event['queryStringParameters']['password']
        # Make some salt, and a bit more stable than os.urandom().
        salt = datetime.today().strftime('%Y-%m-%d').encode()
        # Compute derived key.
        derived_key = do_pbkdf2(password, salt)
        # Build new response.
        response = {
                    "statusCode": 200,
                    "headers": {'Content-Type': 'text/plain'},
                    "body": derived_key
                   }

    LOGGER.info("response: %s", response)
    return response

def do_pbkdf2(password, salt):
    LOGGER.info("do_pbkdf2() started, password: %s", password)
    # Limit password to 1024 bytes.
    bytes = password[:1024].encode()
    # Number of iterations, set to 1M
    n = 1000000

    # Compute key.
    key = hashlib.pbkdf2_hmac('sha256', bytes, salt, n)
    # Hexlify key and decode as string.
    return binascii.hexlify(key).decode()
