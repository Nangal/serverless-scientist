#! /bin/sh
# ----------------------------------------------------------
# Run experiment towards `sort` AWS Lambda function.
# Uses `curl(1)` to send GET request to Lambda.
# ----------------------------------------------------------

# Handle arguments of command.
USAGE="Usage: $0 <number of tests>"
if [ $# == 0 ] ; then
  echo $USAGE
  exit 1;
fi
if ! [[ "$1" =~ ^[0-9]+$ ]]
  then
    echo $USAGE
    exit 1;
fi

# Length of list of random numbers.
SIZE=100

# Number of requests.
times=$1

# Loop $times.
while [ $times -gt 0 ]; do
  # Generate list of random numbers.
  count=1
  while [ "$count" -le $SIZE ]; do
    numbers[$count]="$RANDOM,"
    let "count += 1"
  done
  # Remove trailing comma.
  list=$(echo ${numbers[*]} | sed 's/,$//')
  # Send list to Lambda function with HTTP GET.
  # TODO Obviously, refer only to publicly available URL of Lambda function,
  #      and let Scientist handle calling the candidates.
  # TODO Instead of splitting a string, use an JSON array of values.
  /usr/bin/curl -d "{\"list\":\"${list}\"}" -H "Content-Type: application/json" -X POST https://i3oo9p039k.execute-api.eu-west-1.amazonaws.com/v1/candidate/go1-sort/sort
  echo "\n---"
  /usr/bin/curl -d "{\"list\":\"${list}\"}" -H "Content-Type: application/json" -X POST https://i3oo9p039k.execute-api.eu-west-1.amazonaws.com/v1/candidate/python3-sorted/sort
  echo "\n---"
  /usr/bin/curl -d "{\"list\":\"${list}\"}" -H "Content-Type: application/json" -X POST https://i3oo9p039k.execute-api.eu-west-1.amazonaws.com/v1/candidate/ruby2-sort/sort
  echo

  times=$(($times-1))
  sleep .2
done