#! /bin/sh
# TODO Remove this script once testing Scientist changes is done.
echo "Serverless Scienist quick run test script"

echo "---\ntesting control: sort"
curl -X POST -d "{\"list\": \"1,3,2,4,5\"}" https://jdr5osyr9a.execute-api.eu-west-1.amazonaws.com/v1/sort


echo "\n---\ntesting control: qrcode"
curl -i https://jdr5osyr9a.execute-api.eu-west-1.amazonaws.com/v1/qrcode?text=12345 | head -n 3
echo


echo "\n---\ntesting control: pbkdf2"
time curl https://jdr5osyr9a.execute-api.eu-west-1.amazonaws.com/v1/pbkdf2?password=scientist


echo "\n---\ntesting candidates: sort"
curl -X POST -d "{\"list\": \"1,3,2,4,5\"}" https://gghdy0d6o6.execute-api.eu-west-1.amazonaws.com/v1/candidate/python3-sorted/sort
echo
curl -X POST -d "{\"list\": \"1,3,2,4,5\"}" https://gghdy0d6o6.execute-api.eu-west-1.amazonaws.com/v1/candidate/python3-mergesort/sort
echo
curl -X POST -d "{\"list\": \"1,3,2,4,5\"}" https://gghdy0d6o6.execute-api.eu-west-1.amazonaws.com/v1/candidate/go1-sort/sort
echo


echo "\n---\ntesting candidates: qrcode"
curl -i https://gghdy0d6o6.execute-api.eu-west-1.amazonaws.com/v1/candidate/node8-qr-image/qrcode?text=12345 | head -n 3
echo


echo "\n---\ntesting candidates: pbkdf2"
time curl https://gghdy0d6o6.execute-api.eu-west-1.amazonaws.com/v1/candidate/pbkdf2-256MB/pbkdf2?password=scientist
echo
time curl https://gghdy0d6o6.execute-api.eu-west-1.amazonaws.com/v1/candidate/pbkdf2-1024MB/pbkdf2?password=scientist
echo
time curl https://gghdy0d6o6.execute-api.eu-west-1.amazonaws.com/v1/candidate/pbkdf2-2048MB/pbkdf2?password=scientist


echo "\n---\ntesting experiment: sort"
curl -X POST -d "{\"list\": \"1,3,2,4,5\"}" https://kuj03s6ov9.execute-api.eu-west-1.amazonaws.com/v1/scientist/sort

echo "\n---\ntesting experiment: qrcode"
curl -i https://kuj03s6ov9.execute-api.eu-west-1.amazonaws.com/v1/scientist/qrcode?text=12345 | head -n 3
echo
