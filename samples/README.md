# Samples

This folder contains sample Lambda's voor controls and candidates. The can be ddeployed from the `candidates` and `controls`
directories using and  `sls deploy --profile [your aws profile]` command.

The `traffic` directory contains traffic generators for the examples. The can be deployed using the same `sls deploy` command as above. Do no that generating traffic on the Lambda will generate costs.