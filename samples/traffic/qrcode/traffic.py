import sys
import os
import urllib.request
import urllib.parse
import time
import random
import string
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

# Defaults for target URL, number of requests and delay between requests.
TARGET_URL = ""  # This is silly default, but what is better value?
NR_REQUESTS = 10  # Default: 10 requests each with 1 second delay.
DELAY = 1
MAX_TIME = 120  # Just set some maximum time for traffic burst: 2 minutes.

def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    # Fetch parameters for traffic from ENV.
    try:
        url = os.getenv('QRCODE_TRAFFIC_TARGET_URL', TARGET_URL)
        nr_requests = int(os.getenv('NR_REQUESTS', NR_REQUESTS))
        delay = int(os.getenv('DELAY', DELAY))
        # Generate traffic to URL.
        generate_traffic(url, nr_requests, delay) 
    except:
        LOGGER.error("Error in retrieving URL, number of requests or delay.")

    # Returning a response object isn't necessary for a triggered Lambda function event.
    return

def generate_traffic(url, nr_requests, delay):
    LOGGER.info("generate_traffic() started, URL: %s", url)

    # Test that nr_requests * delay does not exceed MAX_TIME.
    if (nr_requests * delay > MAX_TIME):
        # If so, set them back to defaults.
        nr_requests = NR_REQUESTS
        delay = DELAY

    for _ in range(nr_requests):
        do_one_request(url)
        time.sleep(delay)


def do_one_request(url):
    LOGGER.info("do_one_request() started, URL: %s", url)

    arguments = {}

    # Create a random text to be encoded in QR code. Variable length,
    # variable character set.
    # Small chance that text is too long, if ECL is L or M.
    # QR code byte capacity per ECL is L: 2953, M: 2331, Q: 1663, H: 1273.
    text_length = random.randint(0, 1500)
    # In one of 404 (sic) attempts, pass no string.
    if random.randint(1, 404) > 1:
        arguments["text"] = ''.join(random.choices(string.ascii_letters + string.digits + string.whitespace, k=text_length))

    # Determine format of desired image. One of default, PNG and JPG.
    # 50% chance of default, 40% of PNG, 10% for JPG/JPEG.
    format = random.choices(["", "png", "jpg", "jpeg"], [0.5, 0.4, 0.05, 0.05], k=1)
    if format != [""]:
        arguments["format"] = "".join(format)

    # Determine ECL, one of default, "L", "M", "Q" or "H"
    # 50% chance of default, 15% for L, Q or H, 35% for M.
    random_ecl = random.choice(string.ascii_letters)
    ecl = random.choices(["", "L", "M", "Q", "H", random_ecl], [0.5, 0.05, 0.35, 0.05, 0.04, 0.01], k=1)
    if ecl != [""]:
        arguments["ecl"] = "".join(ecl)
    # 5% chance of wrong level, or lower case.

    # Construct new url.
    qs = urllib.parse.urlencode(arguments)
    # TODO Only include arguments if not null
    url += "?" + qs

    try:
        LOGGER.info("HTTP GET to: %s", url)
        response = urllib.request.urlopen(url).read()
        LOGGER.info("response: %s", response)
    except:
        LOGGER.error("Error in HTTP GET from %s", url)
