import sys
import json
import os
import time
import string
import random
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

# TODO This could be put back, since problem with data format was solved.
# import urllib.request

# import requests  # Not available at AWS Lambda.
from botocore.vendored import requests

# Defaults for target URL, number of requests and delay between requests.
TARGET_URL = ""  # This is silly default.
NR_REQUESTS = 10  # Default: 10 requests each with 1 second delay.
DELAY = 1
MAX_TIME = 120  # Just set some maximum time for traffic burst: 2 minutes.

def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    # Fetch parameters for traffic from ENV.
    try:
        url = os.getenv('SORT_TRAFFIC_TARGET_URL', TARGET_URL)
        nr_requests = int(os.getenv('NR_REQUESTS', NR_REQUESTS))
        delay = int(os.getenv('DELAY', DELAY))
        # Generate traffic to URL.
        generate_traffic(url, nr_requests, delay) 
    except:
        print("Error in retrieving URL, number of requests or delay.")

    # Returning a response object isn't necessary for a triggered Lambda function event.
    return

def generate_traffic(url, nr_requests, delay):
    LOGGER.info("generate_traffic() started, URL: %s", url)

    # Test that nr_requests * delay does not exceed MAX_TIME.
    if (nr_requests * delay > MAX_TIME):
        # If so, set them back to defaults.
        nr_requests = NR_REQUESTS
        delay = DELAY

    for _ in range(nr_requests):
        do_one_request(url)
        time.sleep(delay)


def do_one_request(url):
    LOGGER.info("do_one_request() started, URL: %s", url)

    # Get a random-sized list of random strings.
    nr_items = random.randint(10,50)
    unsorted_strings = []
    allowed_chars = string.ascii_letters + string.digits
    for _ in range(nr_items):
        string_length = random.randint(4,16)
        random_string = ''.join([random.choice(allowed_chars) for n in range(string_length)])
        unsorted_strings.append(random_string)

    unsorted_list = ",".join(unsorted_strings)

    # Construct request body.
    data = {"list": unsorted_list}

    # json_data = json.dumps(data) # .encode('ascii') # Needs to be UTF-8 encoded.

    try:
        # Prepare the request and send it.
        # TODO We could use 'normal' urllib.request again.
        LOGGER.info("HTTP POST to: %s", url)
        response = requests.post(url, json = data)
        LOGGER.info("response: %s", response)
    except:
        LOGGER.error("Error in HTTP POST to %s", url)
