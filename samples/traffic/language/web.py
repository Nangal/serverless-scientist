import sys
import os
import logging
from string import Template

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

# Defaults for web URL endpoints.
TEMPLATE_FILE = "language/web/index.html"


def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    response = {"statusCode": 404, "body": "Not Found."}

    # Fetch parameters from ENV.
    try:
        hosts = {
            "scientist_host": os.getenv(
                "SCIENTIST_HOST", "https://api.serverlessscientist.com"
            ),
            "control_host": os.getenv(
                "CONTROL_HOST", "https://controls.serverlessscientist.com"
            ),
            "candidates_host": os.getenv(
                "CANDIDATES_HOST", "https://candidates.serverlessscientist.com"
            ),
        }

        # Fetch HTML of index page.
        with open(TEMPLATE_FILE) as f:
            template_html = f.read()

        # Substitute variables in template by actual values.
        template = Template(template_html)
        html = template.safe_substitute(hosts)

        response = {
            "statusCode": 200,
            "headers": {"Content-Type": "text/html"},
            "body": html,
        }
    except:
        LOGGER.exception("Error in retrieving requested file.")

    return response


def fetch_html(name):
    # Try to open file.

    # Return file's contents.
    return "<html><body>Hello, World!</body></html>"
