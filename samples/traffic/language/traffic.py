import feedparser
import sys
import os
import urllib.request
import urllib.parse
import time
import random
import string
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

# Defaults for target URL, number of requests and delay between requests.
TARGET_URL = ""  # This is silly default, but what is better value?
NR_REQUESTS = 2  # Default: 2 requests with 10 second delay.
DELAY = 10
MAX_TIME = 120   # Just set some maximum time for traffic burst: 2 minutes.

def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    # Fetch parameters for traffic from ENV.
    try:
        url = os.getenv('LANGUAGE_TRAFFIC_TARGET_URL', TARGET_URL)
        nr_requests = int(os.getenv('NR_REQUESTS', NR_REQUESTS))
        delay = int(os.getenv('DELAY', DELAY))
        # Generate traffic to URL.
        generate_traffic(url, nr_requests, delay) 
    except:
        LOGGER.error("Error in retrieving URL, number of requests or delay.")

    # Returning a response object isn't necessary for a triggered Lambda function event.
    return

def generate_traffic(url, nr_requests, delay):
    LOGGER.info("generate_traffic() started, URL: %s", url)

    # Test that nr_requests * delay does not exceed MAX_TIME.
    if (nr_requests * delay > MAX_TIME):
        # If so, set them back to defaults.
        nr_requests = NR_REQUESTS
        delay = DELAY

    for _ in range(nr_requests):
        do_one_request(url)
        time.sleep(delay)

FEEDS = [  # A list of RSS feeds of popular European newspapers.
        'https://www.volkskrant.nl/voorpagina/rss.xml',      # de Volkskrant, Dutch
        'https://www.lemonde.fr/rss/une.xml',                # Le Monde, French
        'https://www.welt.de/feeds/latest.rss',              # Die Welt, German
        'http://www.repubblica.it/rss/homepage/rss2.0.xml',  # Repubblica, Italian
        'https://elpais.com/rss/elpais/portada.xml',         # El País, Spanish
        'https://www.theguardian.com/uk/rss',                # The Guardian, English
        'https://www.aftenposten.no/rss',                    # Aftenposten, Norsk
        'https://www.dn.se/rss/',                            # Dagens Nyheter, Swedish
        'https://www.hs.fi/rss/tuoreimmat.xml',              # Helsingin Sanomat, Finnish
        'https://www.berlingske.dk/content/rss'              # Berlingske, Danish
        ]

def do_one_request(url):
    LOGGER.info("do_one_request() started, URL: %s", url)

    # Pick a random newspaper RSS feed.
    rss_feed = random.choice(FEEDS)

    # Read the feed, and pick a random entry.
    feed = feedparser.parse(rss_feed)
    entry = random.choice(feed.entries)

    # Get the title and the description.
    # title = entry.title
    description = entry.description

    # Create a text string.
    arguments = {}
    arguments["text"] = description[:1000]

    # Construct new url.
    qs = urllib.parse.urlencode(arguments)
    url += "?" + qs

    try:
        LOGGER.info("HTTP GET to: %s", url)
        response = urllib.request.urlopen(url).read()
        LOGGER.info("response: %s", response)
    except:
        LOGGER.error("Error in HTTP GET from %s", url)
