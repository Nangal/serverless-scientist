exports.lambdaHandler = function (event, context, callback) {
  console.log("[INFO] lambdaHandler() started, event: ", event)

  // Create default response.
  response = {
      'statusCode': 404,
      'headers': {'Content-Type': 'application/json'},        
      'body': JSON.stringify({"error": "Pass a JSON object with a list argument."})
  }

  // TODO Instead of interpreting `list` as string, make it a proper JSON list.
  if (event.body !== null && event.body !== undefined) {
    var jsonObject 
    // Because of setting of binary media types in API Gateway, AWS Lambda
    // received incoming JSON strings encoded as base 64. If so, then we decode it.
    // TODO Obviously, this magic also needs to happen in candidates.
    if (event.isBase64Encoded) {
      jsonObject = JSON.parse(Buffer.from(event.body, 'base64').toString())
    } else {
      jsonObject = JSON.parse(event.body)
    }

    if (jsonObject.list) {
      list = jsonObject.list;
      sortedList = do_sort(list);

      response = {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},        
        'body': JSON.stringify({
                list: list,
                sorted_list: sortedList
            })
      }
    }
  }

  console.log("[INFO] response: ", response)
  callback(null, response);
};

function do_sort(list) {
  console.log("[INFO] do_sort() started, list: ", list)

  return list.split(",").sort().join(",");
}