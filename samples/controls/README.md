# Controls

TODO: Input / request data, and response can be 
described a bit more eloborately.

## Control: /round

Node.js implementation to round a floating-point
number passed as query string parameter to HTTP GET
request. Uses JavaScript's native `Math.round()`.

## Control: /sort

Node.js implementation to sort a comma-separated list
of strings, passed as request body in HTTP POST request. 
Uses JavaScript's native `sort()`.

## Control: /qrcode

Node.js implementation to generate a QR code for a 
text string passed as a query string parameter
to a HTTP GET request. Uses library `qrcode` to
generate the QR code image.
