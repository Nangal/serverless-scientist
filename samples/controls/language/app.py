import langdetect
import sys
import os
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    # Create error response in case password is missing in query string.
    response = {
        "statusCode": 404,
        # TODO Restrict origin to more strict domain.
        "headers": {"Access-Control-Allow-Origin": "*", "Content-Type": "text/plain"},
        "body": "Supply a piece of text as query string parameter, e.g., `?text=it+should+detect+this+is+in+English`.",
    }

    if event["queryStringParameters"] and "text" in event["queryStringParameters"]:
        # Get text from event data.
        text = event["queryStringParameters"]["text"][:1000]
        language = do_language_detection(text)
        # Build new response.
        response = {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "text/plain",
            },
            "body": language,
        }

    LOGGER.info("response: %s", response)
    return response


def do_language_detection(text):
    LOGGER.info("do_language_detection() started, text: %s", text)

    try:
        language = langdetect.detect(text)
    except:
        language = ""

    return language
