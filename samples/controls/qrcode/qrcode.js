var QRCode = require('qrcode')

exports.lambdaHandler = function (event, context, callback) {
  console.log("[INFO] lambdaHandler() started, event: ", event)

  // Prepare default response that'll be used when request is incorrect.
  response = {
      'statusCode': 404,
      'headers': {'Content-Type': 'application/json'},        
      'body': JSON.stringify({"error": "Pass correct arguments for text, and optionally format and error correction level."})
  }

  // Check required query string parameter `text`.
  if (!event.queryStringParameters ||
      !event.queryStringParameters.text) {
    callback(null, response)
    return
  }

  // Get text to be encoded in QR code.
  text = event.queryStringParameters.text

  // Set requested format, "png" as default.
  format = "image/png"

  if (event.queryStringParameters.format) { // TODO Get it from URL path, not from query string.
      if (event.queryStringParameters.format == "jpg" || event.queryStringParameters.format == "jpeg" ) {
        format = "image/jpeg"
      }
  }

  headers = {}
  headers["Content-Type"] = format

  // Take default "byte" for encoding.
  mode = "byte" // Alternatively, "numeric", "alphanumeric", "kanji".

  // Get error correction level from request, take "M" as default.
  ecl = "M" // Can be "L", "M", "Q" or "H".

  if (event.queryStringParameters.ecl) {
      if (event.queryStringParameters.ecl.toUpperCase() == "L" ||
          event.queryStringParameters.ecl.toUpperCase() == "Q" ||
          event.queryStringParameters.ecl.toUpperCase() == "H") {
        ecl = event.queryStringParameters.ecl.toUpperCase()
      }
  }

  // QR code will be stored in base64-encoded data URL.
  dataURL = null
  options = {}
  options["type"] = format
  options["errorCorrectionLevel"] = ecl

  QRCode.toDataURL(text, options, function (err, url) {
    console.log("[INFO] QRCode.toDataURL() done, text: ", text)
    // Text might be to large to code in QR code. Then `err` will be set.
    if (!err) {
      dataURL = url

      // Remove header to get base64-encoded string of image.
      data = dataURL.split(';base64,').pop()

      // Build response.
      response = {
        // Necessary so that API Gateway transforms it to binary.
        'isBase64Encoded': true, 
        'statusCode': 200,
        'headers': headers,
        'body': data  // No need to JSON.stringify() data.
      }
    }

    console.log("[INFO] response: ", response)
    callback(null, response);
  })
};