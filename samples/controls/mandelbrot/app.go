package main

import (
	"bytes"
	"encoding/base64"
	"image"
	"image/color"
	"image/png"
	"io"
	"math/cmplx"
	"net/http"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func lambdaHandler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// Set default values for x, y and length.
	var xmin, ymin, length = -4.5, -2.5, 9.0

	// Get x, y and length from HTTP GET request query string.
	x, err := strconv.ParseFloat(request.QueryStringParameters["x"], 64)
	if err == nil {
		xmin = x
	}

	y, err := strconv.ParseFloat(request.QueryStringParameters["y"], 64)
	if err == nil {
		ymin = y
	}

	l, err := strconv.ParseFloat(request.QueryStringParameters["l"], 64)
	if err == nil {
		length = l
	}

	// Generate Mandelbrot image.
	img := generateMandelbrot(xmin, ymin, length)

	// Convert image to PNG image in byte buffer.
	var buffer bytes.Buffer
	err = png.Encode(io.Writer(&buffer), img)

	// Return error if PNG encoding didn't work.
	if err != nil {
		return events.APIGatewayProxyResponse{
			Body:       "Error in encoding PNG image.",
			StatusCode: http.StatusInternalServerError}, nil
	}

	// Convert image to base64 encoded string.
	imgString := base64.StdEncoding.EncodeToString(buffer.Bytes())

	return events.APIGatewayProxyResponse{
		Body:            imgString,
		StatusCode:      http.StatusOK,
		Headers:         map[string]string{"content-type": "image/png"},
		IsBase64Encoded: true}, nil
}

// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/
// generateMandelbrot returns an image of the Mandelbrot fractal.
func generateMandelbrot(xmin, ymin, length float64) image.Image {
	const (
		width  = 1600
		ratio  = 16.0 / 9.0
		height = width / ratio
	)

	img := image.NewRGBA(image.Rect(0, 0, width, height))

	for py := 0; py < height; py++ {
		y := float64(py)/height*(length/ratio) + ymin

		for px := 0; px < width; px++ {
			x := float64(px)/width*(length) + xmin
			z := complex(x, y)
			img.Set(px, py, mandelbrot(z))
		}
	}
	return img
}

func mandelbrot(z complex128) color.Color {
	const iterations = 200
	const contrast = 15

	var v complex128
	for n := uint8(0); n < iterations; n++ {
		v = v*v + z
		if cmplx.Abs(v) > 2 {
			// TODO Read https://www.codingame.com/playgrounds/2358/how-to-plot-the-mandelbrot-set/adding-some-colors
			return color.Gray{255 - contrast*n}
		}
	}
	return color.Black
}

func main() {
	lambda.Start(lambdaHandler)
}
