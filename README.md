# README

Serverless Scientist is based on Githubs Scientist approach. For more background refer to:

* GitHub [articles](https://github.blog/2015-12-15-move-fast/) on [Scienttist](http://github.blog/2016-02-03-scientist/]) approach.
* [Scientist, a novel software QA method](https://xebia.com/blog/scientist-a-novel-software-qa-method/) blog post
* [Serverless Scientist](https://xebia.com/blog/serverless-scientist) blog post
* [How to reduce AWS Lambda latency using custom runtimes](https://xebia.com/blog/how-to-reduce-aws-lambda-latency-using-custom-runtimes/) blog post


TODO: Make README a bit more complete, explaining the `scientist/`
and `samples/candidates/` and `samples/controls` folders:

* `scientist` to deploy the scientist project and use it
* `samples/controls` to deploy some 'existing' applications ready to be improved
* `samples/candidates` to deploy the sample experiments
* `samples/traffic` to deploy the traffic generators

## Installation

-> TODO: Refer to `INSTALLATION.md` file.

Install latest serverless version for offline / development use:

```bash
npm install
npm install serverless-offline-python
serverless offline
```

TODO: Describe installation/deployment per project folder.

## Testing

Testing the installation can we done with `curl localhost:3000/round/control?number=0.42`.

TODO That ^ is just testing one endpoint. There are more functions exposed and available
for testing.

For online / production use, this should do it:

```bash
npm install
serverless deploy # TODO Mention requirements w.r.t. AWS credentials, profile, region and such.
```

The installation can be tested with `curl some-URL-as-API-endpoint-for-AWS-Lambdas/round/control?number=0.42`.

## Using Serverless Scientist

TODESCRIBE

## Quick hack to run Grafana/Graphite

Clone this project `https://github.com/kamon-io/docker-grafana-graphite.git`
and run `make up`.

After that, data can be published to it and you can configure your dashboards.
